The algorithm for this script is based on Hashicorp's, available at
<https://github.com/hashicorp/vault/tree/b69998de5fb38965e0f71d3e40c2fc5d3fadba35/shamir>
and licensed under the Mozilla Public License, version 2.0.
