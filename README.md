# ls4.d -- Shamir's Secret Sharing in D

This is an attempt to get Shamir's Secret Sharing to work on arbitrary
amount of data in a manner which is simple to understand code-side and
does not sacrifice information security.

In order to maintain simplicity, the code will forego most optimizations
and hardening.
